<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            .text-center {
                text-align: center;
            }

            .upload-form {
                height: 20px;
                padding: 20px 0;
            }

            .image {
                margin: 10px;
            }

        </style>
    </head>
    <body>
        <div class="text-center upload-form">
            <form action="{{ route('upload') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="image">
                <button type="submit"> Upload </button>
            </form>
        </div>
        <div class="text-center image-wrapper">
            @foreach($images as $image)
                <img src="{{ url('storage/' . $image) }}" class="image" />
            @endforeach
        </div>
    </body>
</html>
