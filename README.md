## Start project

- Install Docker and docker-compose
- Execute `docker-compose --compatibility up -d`
- Follow the link to open the project: [http://127.0.0.1:8200](http://127.0.0.1:8200)
