<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\ImageService;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private const MAX_IMAGE_SIZE = 1024 * 64; // 64 Mb

    /**
     * @var ResponseFactory
     */
    private $response;

    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @param ResponseFactory $response
     * @param ImageService $imageService
     */
    public function __construct(
        ResponseFactory $response,
        ImageService $imageService
    ) {
        $this->response = $response;
        $this->imageService = $imageService;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $images = $this->imageService->getImagesWithWatermark();

        return $this->response->view('welcome', ['images' => $images]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws ValidationException
     */
    public function upload(Request $request): Response
    {
        $this->validate($request, [
            'image' => 'required|image|max:' . HomeController::MAX_IMAGE_SIZE,
        ]);

        $this->imageService->uploadImage($request->file('image'));

        return $this->response->redirectToRoute('home');
    }
}
