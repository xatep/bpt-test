<?php

namespace App\Providers;

use App\Utils\ImageUploader\ImageUploader;
use App\Utils\ImageUploader\StorageImageUploader;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\ImageManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ImageUploader::class, StorageImageUploader::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
