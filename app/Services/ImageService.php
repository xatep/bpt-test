<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use Illuminate\Contracts\Filesystem\Factory as FilesystemManager;

class ImageService
{
    private const WATERMARKS_PATH = 'watermarks/';
    private const IMAGES_PATH = 'storage/';

    /**
     * @var ImageManager
     */
    private $imageManager;

    /**
     * @var FilesystemManager
     */
    private $filesystemManager;

    /**
     * @param ImageManager $imageManager
     * @param FilesystemManager $filesystemManager
     */
    public function __construct(
        ImageManager $imageManager,
        FilesystemManager $filesystemManager
    ) {
        $this->imageManager = $imageManager;
        $this->filesystemManager = $filesystemManager;
    }

    public function getImagesWithWatermark(): array
    {
        $files = $this->filesystemManager->disk()->allFiles();

        return array_filter($files, function (string $filename) {
            return strpos($filename, '.water.');
        });
    }

    /**
     * @param UploadedFile $image
     */
    public function uploadImage(UploadedFile $image): void
    {
        $imageForWatermark = $this->imageManager->make($image);

        list($red, $green, $blue, ) = (clone $imageForWatermark)->limitColors(1)->pickColor(0, 0);

        $waterMarkColor = $this->ensureWatermarkColorForRGB($red, $green, $blue);

        $waterMark = $this->imageManager
            ->make(ImageService::WATERMARKS_PATH . $waterMarkColor . '.jpg')
            ->encode('png')
            ->limitColors(2, '#ffffff')
            ->trim();

        $imageForWatermark->insert($waterMark, 'center');

        $imageForWatermark->save(sprintf(
            '%s%s.water.%s',
            ImageService::IMAGES_PATH,
            $this->generateFilename(),
            $image->getClientOriginalExtension()
        ));
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     *
     * @return string
     */
    private function ensureWatermarkColorForRGB(int $red, int $green, int $blue): string
    {
        if ($red > $green && $red > $blue) {
            return 'black';
        }

        if ($green > $red && $green > $blue) {
            return 'red';
        }

        return 'yellow';
    }

    /**
     * Generate unique name
     *
     * @param int $length
     * @return string
     */
    private function generateFilename(int $length = 16): string
    {
        $name = Str::random($length);

        if (!$this->filesystemManager->disk()->exists($name)) {
            return $name;
        };

        return $this->generateFilename($length);
    }
}
