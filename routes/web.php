<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');
Route::post('/images', 'HomeController@upload')->name('upload');
